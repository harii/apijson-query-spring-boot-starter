# 插件介绍

*一个快速构建apiJson查询条件的插件*

通常我们在数仓或者其他场景中会使用apijson提供通用接口，这时候会有其他应用需要调用apijson接口，虽然apijson提供通用的json格式参数调用，
但是由于apijson单独实现一套语法规则，需要学习成本，此插件的目的就是像使用mybatis
plus构建sql查询参数一样，去构建apijson查询条件，无需任何apijson语法学习，即可完成通用查询请求。

# 查询构建使用示例

* 基于ApiJsonQueryLambdaWrapper构建查询

```java
public class Test {
    public void testQuery() {
        ApiJsonQueryLambdaWrapper queryWrapper = ApiJsonQueryWrappers.lambdaQuery()
                .setSchema("模式名")
                .setTableName("表名")
                .setBiSigns("业务标识")
                .select("查询列")
                .eq("条件判断", "条件列", "条件值")
                // ...
                ;
    }
}
```

* 基于ApiJsonQueryStringWrapper构建查询

```java
public class Test {
    public void testQuery() {
        ApiJsonQueryStringWrapper queryWrapper = ApiJsonQueryWrappers.query()
                .setSchema("模式名")
                .setTableName("表名")
                .setBiSigns("业务标识")
                .select("查询列")
                .eq("条件判断", "条件列", "条件值")
                //...
                ;
    }
}
```

* 基于ApiJsonQueryMapWrapper构建查询

```java
public class Test {
    public void testQuery() {
        Map<String, Object> param = new HashMap<>();
        //apijson原生语法
        param.put("键", "值");
        //...
        ApiJsonQueryMapWrapper queryWrapper = ApiJsonQueryWrappers.mapQuery()
                .setQueryParam(param);
    }
}
```

# 方法调用使用示例

提供ApiJsonQueryTemplate模板方法

* 查询一条数据 getOne(查询条件,转换类型)

```java
public class Test {

    @Autowired
    private ApiJsonQueryTemplate apiJsonQueryTemplate;

    public void testGetOne() {

        User user = apiJsonQueryTemplate.getOne(queryWrapper, User.class);

    }
}
```

* 查询条数 count(查询条件)

```java
public class Test {

    @Autowired
    private ApiJsonQueryTemplate apiJsonQueryTemplate;

    public void testCount() {

        Long count = apiJsonQueryTemplate.count(queryWrapper);

    }
}
```

* 查询列表 getList(查询条件,转换类型)

```java
public class Test {

    @Autowired
    private ApiJsonQueryTemplate apiJsonQueryTemplate;

    public void testGetList() {

        List<User> userList = apiJsonQueryTemplate.getList(queryWrapper, User.class);

    }
}
```

* 查询分页列表 getPageList(查询条件,当前页,页大小,转换类型)

```java
public class Test {

    @Autowired
    private ApiJsonQueryTemplate apiJsonQueryTemplate;

    public void testGetPageList() {

        ApiJsonPageInfo<User> userPageList = apiJsonQueryTemplate.getPageList(queryWrapper, 1, 10, User.class);

    }
}
```

# 扩展查询数据源示例

提供默认基于http形式的apijson数据源查询方法，如果不满足需求，可继承DwQueryDao接口,实现getData方法，然后再装配Bean

```java
public class AutoConfiguration {

    @Resource
    private  ApiJsonQueryProperties apiJsonQueryProperties;

    @Bean
    public ApiJsonQueryTemplate apiJsonQueryTemplate() {
        log.debug("加载自定义查询数据源实现....");
        ApiJsonQueryTemplate apiJsonQueryTemplate = new ApiJsonQueryTemplate();
        //自定义查询数据源
        ApiJsonQueryDao customQuery = new CustomQueryImpl(apiJsonQueryProperties);
        apiJsonQueryTemplate.setDao(customQuery);
        return apiJsonQueryTemplate;
    }


}
```

# 注解说明
* ApiJsonTableName 表名注解
```java
@ApiJsonTableName(
        value = "表名",
        schema = "模式名",
        biSigns = "业务标识",
        desc = "描述"
)
public class TestDO{
    
}

//注解设置完成后，只需要在构建查询条件时 setTableName(TestDO.class)即可
```
* ApiJsonTableField 字段名注解

```java
@ApiJsonTableName(
        value = "表名",
        schema = "模式名",
        biSigns = "业务标识",
        desc = "描述"
)
public class TestDO {

    //将name映射为name2查询
    @ApiJsonTableField(value = "name2")
    private String name;

    //忽略该字段查询
    @ApiJsonTableField(exist = false)
    private String value;

}

//注解设置完成后，只需要在构建查询条件时 setTableName(TestDO.class)即可
```
# 使用说明
在maven中加入以下依赖：
```text
<dependency>
    <groupId>io.gitee.mingbaobaba</groupId>
    <artifactId>apijson-query-spring-boot-starter</artifactId>
    <version>版本</version>
</dependency>
```
package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

import lombok.Data;

import java.util.List;

/**
 * <p>数仓分页返回</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/20 22:47
 */
@Data
public class ApiJsonPageInfo<T> {
    private List<T> data;
    private long total;
    private long page;
    private long limit;
}

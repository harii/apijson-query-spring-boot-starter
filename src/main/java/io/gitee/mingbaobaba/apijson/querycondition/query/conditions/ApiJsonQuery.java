package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

import java.io.Serializable;
import java.util.Map;

/**
 * <p>构建数仓查询接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/19 18:03
 */

public interface ApiJsonQuery extends Serializable {

    /**
     * 获取参数
     *
     * @return Map<String, Object
     */
    Map<String, Object> getApiJsonParam();

    /**
     * 获取模式名
     *
     * @return 模式
     */
    String getSchema();

    /**
     * 获取表名
     *
     * @return 表名
     */
    String getTable();

    /**
     * 获取业务标识
     *
     * @return 业务标识
     */
    String getBiSigns();

    /**
     * 清空条件
     */
    void clear();

}

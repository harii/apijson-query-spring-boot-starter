package io.gitee.mingbaobaba.apijson.querycondition.query.exception;

/**
 * <p>条件操作异常类</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/7/10 13:48
 */
public class ConditionException extends RuntimeException {
    public ConditionException() {
    }

    public ConditionException(String message) {
        super(message);
    }

    public ConditionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConditionException(Throwable cause) {
        super(cause);
    }

    public ConditionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

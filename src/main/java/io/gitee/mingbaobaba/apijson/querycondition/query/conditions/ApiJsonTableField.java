package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

import java.lang.annotation.*;

/**
 * <p>字段名</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/21 9:25
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiJsonTableField {

    String value() default "";

    boolean exist() default true;
}

package io.gitee.mingbaobaba.apijson.querycondition.configuration;

import io.gitee.mingbaobaba.apijson.querycondition.query.dao.impl.DefaultHttpApiJsonQueryDaoImpl;
import io.gitee.mingbaobaba.apijson.querycondition.query.properties.ApiJsonQueryProperties;
import io.gitee.mingbaobaba.apijson.querycondition.query.template.ApiJsonQueryTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * <p>自动配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/7/5 10:43
 */
@Slf4j
@ComponentScan(basePackages = "io.gitee.mingbaobaba.apijson.querycondition.query")
public class AutoConfiguration {

    @Resource
    private ApiJsonQueryProperties dataWarehouseProperties;

    @PostConstruct
    public void postConstruct() {
        log.debug("ApiJson Configuration Start");
    }

    @Bean
    @ConditionalOnMissingBean
    public ApiJsonQueryTemplate apiJsonQueryTemplate() {
        log.debug("加载默认ApiJson查询实现....");
        ApiJsonQueryTemplate apiJsonQueryTemplate = new ApiJsonQueryTemplate();
        apiJsonQueryTemplate.setDao(new DefaultHttpApiJsonQueryDaoImpl(dataWarehouseProperties));
        return apiJsonQueryTemplate;
    }


}

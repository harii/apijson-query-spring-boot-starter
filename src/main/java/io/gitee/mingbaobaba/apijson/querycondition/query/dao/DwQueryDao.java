package io.gitee.mingbaobaba.apijson.querycondition.query.dao;

import java.util.Map;

/**
 * <p>数仓查询接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/27 20:35
 */
public interface DwQueryDao {

    /**
     * 获取数据接口
     *
     * @param params  参数
     * @param biSigns 业务标识
     * @return String
     */
    String getData(Map<String, Object> params, String biSigns);

}

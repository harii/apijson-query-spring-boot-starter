package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * <p>条件</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/21 8:56
 */
@Data
@AllArgsConstructor
public class Condition {

    /**
     * 列名
     */
    private String column;
    /**
     * 连接关键字
     */
    private EnumKeyword keyword;

    /**
     * 值
     */
    private Object val;
}

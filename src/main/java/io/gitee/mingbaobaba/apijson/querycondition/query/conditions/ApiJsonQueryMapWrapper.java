package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>基于Map形式构建查询参数</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/20 5:18
 */
public class ApiJsonQueryMapWrapper<T> extends AbstractBaseWrapper<T, String, ApiJsonQueryMapWrapper<T>> implements ApiJsonQuery {

    private final Map<String, Object> mapParams = new HashMap<>();

    public ApiJsonQueryMapWrapper<T> setQueryParam(Map<String, Object> params) {
        mapParams.putAll(params);
        return typedThis;
    }

    @Override
    protected void buildParams() {
        super.buildParams();
        if (!CollectionUtils.isEmpty(mapParams)) {
            apiJsonParams.putAll(mapParams);
        }
    }
}
